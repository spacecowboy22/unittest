<?php

namespace SpaceCowboy;

use DateTime;

class Todolist
{

    public function __construct(User $user)
    {
        $this->user = $user;
        $this->items = [];
    }

    public function canAddNewItem()
    {
        return count($this->items) >= 0 && count($this->items) < 10 ;
    }


    public function addItem(Item $item)
    {
        $email = new EmailService();
        $email->send($this->user);
        array_push($this->items,$item);
    }

    public function isItemUnique(Item $newItem)
    {
        $result = true;
        foreach ($this->items as $item) {
            if($item->name == $newItem->name)
                $result = false;
        }

        return $result;
    }

    public function diffTimeItem()
    {
        $currentTime = new DateTime("now");

        if(count($this->items) == 0)
            return true;

        $lastItem = end($this->items);

        $lastItemTime = $lastItem->createdAt;

        $lastItemTime->modify("+30 minutes");

        return $currentTime >= $lastItemTime;

    }


    public function canAddItem(Item $item)
    {
        if($this->canAddNewItem() && $this->isItemUnique($item) && $this->diffTimeItem())
            return $item;

        else
            return null ;
    }
}