<?php

namespace SpaceCowboy;

use DateTime;

class Item
{
    public function __construct(string $name, string $content)
    {
        $this->name = $name;
        $this->content = $content;
        $this->createdAt = new DateTime("now");
    }

    public function isContentValid()
    {
        return $this->content <= 1000 ;
    }


}