<?php

namespace SpaceCowboy;

class User
{
    public function __construct(string $firstname, string $lastname, float $age, string $email, string $password)
    {
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->age = $age;
        $this->email = $email;
        $this->password = $password;
        $this->hastodolist = false;
        $this->todolist = null;
    }


    public function isValidEmail()
    {
        return !empty($this->email) && filter_var($this->email, FILTER_VALIDATE_EMAIL);
    }


    public function isValidFullName()
    {
        return !empty($this->firstname) && !empty($this->lastname) ;

    }

    public function isValidAge()
    {
        return $this->age >= 13;
    }

    public function isValidPassword()
    {
        return !empty($this->password) && strlen($this->password) > 8 && strlen($this->password) < 40 ; 
    }

    public function isValid()
    {
        return $this->isValidFullName() && $this->isValidAge() && $this->isValidPassword() && $this->isValidEmail();
    }

    public function hasTodoList()
    {
        return $this->hastodolist;
    }

    public function createTodoList()
    {
        if($this->hasTodoList())
            return false;

        $this->todolist = new Todolist($this);
        $this->hastodolist = true;

        return true;

    }


}