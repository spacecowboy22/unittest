<?php


use SpaceCowboy\Item;
use SpaceCowboy\Todolist;
use SpaceCowboy\User;

class TodoListTest  extends PHPUnit\Framework\TestCase
{

    //Item's Tests

    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->user = new User("samuel","benitah",23,"sambenitah@gmail.com","samsamsam");
    }

    public function testCanAddItem()
    {   $todoList = new Todolist($this->user);
        $this->assertEquals(true,$todoList->canAddNewItem(),"Return true if TodoList is between 0 and 10 items");
    }

    public function testCantAddItem()
    {   $todoList = new Todolist($this->user);
        for($i = 0; $i<10; $i++)
        {
            $todoList->addItem(new Item("name".$i , "content".$i));
        }
        $this->assertEquals(false,$todoList->canAddNewItem(),"Return false if TodoList has 10 items or more");
    }

    public function testIsItemUnique()
    {   $todoList = new Todolist($this->user);
        $todoList->addItem(new Item("name", "content"));
        $this->assertEquals(true,$todoList->isItemUnique(New Item("name1","content")),"Return true if item is unique");
    }

    public function testIsItemNotUnique()
    {   $todoList = new Todolist($this->user);
        $todoList->addItem(new Item("name", "content"));
        $this->assertEquals(false,$todoList->isItemUnique(New Item("name","content")),"Return true if item is unique");
    }

    public function testValidDiffTime()
    {
        $todoList = new Todolist($this->user);
        $item = new Item("name", "content");
        $item->createdAt->modify("-30 minutes");
        $todoList->addItem($item);
        $this->assertEquals(true,$todoList->diffTimeItem(),"Return true if diff time is higher or equals to 30 mn");
    }

    public function testValidDiffTimeWithEmptyList()
    {
        $todoList = new Todolist($this->user);
        $this->assertEquals(true,$todoList->diffTimeItem(),"Return true if diff time TodoList is empty");
    }

    public function testUnValidDiffTime()
    {
        $todoList = new Todolist($this->user);
        $item = new Item("name", "content");
        $todoList->addItem($item);
        $this->assertEquals(false,$todoList->diffTimeItem(),"Return false if diff time is lower to 30 mn");
    }


    public function testAddItem()
    {
        $todoList = new Todolist($this->user);
        $item = new Item("name", "content");
        $item->createdAt->modify("-30 minutes");
        $newitem = new Item("name1","content");
        $todoList->addItem($item);
        $this->assertEquals($newitem,$todoList->canAddItem($newitem),"Return the item if he has been added");
    }


    public function testCannotAddItem()
    {
        $todoList = new Todolist($this->user);
        $item = new Item("name", "content");
        $newitem = new Item("name1","content");
        $todoList->addItem($item);
        $this->assertEquals(null,$todoList->canAddItem($newitem),"Return null if the item cant be added");
    }

}