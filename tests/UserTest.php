<?php


use SpaceCowboy\User;

class Usertest extends PHPUnit\Framework\TestCase
{
    //Password's Tests

    public function testValidPassword()
    {
        $user = new User("samuel","benitah",23,"sambenitah@gmail.com","samsamsam");
        $this->assertEquals(true,$user->isValidPassword(),"Return true if password is between 8 and 40 characters");
    }

    public function testUnValidPassword()
    {
        $user = new User("samuel","benitah",23,"sambenitah@gmail.com","ss");
        $this->assertEquals(false,$user->isValidPassword(),"Return false if password is not between 8 and 40 characters");
    }

    public function testEmptyPassword()
    {
        $user = new User("samuel","benitah",23,"sambenitah@gmail.com","");
        $this->assertEquals(false,$user->isValidPassword(),"Return false if password is empty");
    }


    //Email's Tests

    public function testValidEmail()
    {
        $user = new User("samuel","benitah",23,"sambenitah@gmail.com","samsamsam");
        $this->assertEquals(true,$user->isValidEmail(),"Return true if email is valid");
    }

    public function testUnValidEmail()
    {
        $user = new User("samuel","benitah",23,"sambenitahgmailcom","samsamsam");
        $this->assertEquals(false,$user->isValidEmail(),"Return false if email is not valid");
    }

    public function testEmptyEmail()
    {
        $user = new User("samuel","benitah",23,"","samsamsam");
        $this->assertEquals(false,$user->isValidEmail(),"Return false if email is empty");
    }

    //Age's Tests

    public function testHigherValidAge()
    {
        $user = new User("samuel","benitah",23,"sambenitah@gmail.com","samsamsam");
        $this->assertEquals(true,$user->isValidAge(),"Return true if age is higher than 13");
    }

    public function testEqualsValidAge()
    {
        $user = new User("samuel","benitah",13,"sambenitah@gmail.com","samsamsam");
        $this->assertEquals(true,$user->isValidAge(),"Return true if age is 13");
    }

    public function testUnValidAge()
    {
        $user = new User("samuel","benitah",12,"sambenitah@gmail.com","samsamsam");
        $this->assertEquals(false,$user->isValidAge(),"Return false if age is lower than 13");
    }

    public function testEmptyAge()
    {
        $user = new User("samuel","benitah",12,"sambenitah@gmail.com","samsamsam");
        $this->assertEquals(false,$user->isValidAge(),"Return false if age is empty");
    }

    //LastNama & FirstName Tests

    public function testValidFullName()
    {
        $user = new User("samuel","benitah",23,"sambenitah@gmail.com","samsamsam");
        $this->assertEquals(true,$user->isValidFullName(),"Return true if firstname and lastname are both valid");
    }

    public function testEmptyFirstName()
    {
        $user = new User("","benitah",23,"sambenitah@gmail.com","samsamsam");
        $this->assertEquals(false,$user->isValidFullName(),"Return false if firstname is empty and lastname is valid");
    }

    public function testEmptyLastName()
    {
        $user = new User("samuel","",23,"sambenitah@gmail.com","samsamsam");
        $this->assertEquals(false,$user->isValidFullName(),"Return false if firstname is valid and lastname is empty");
    }

    public function testEmptyFullName()
    {
        $user = new User("","",23,"sambenitah@gmail.com","samsamsam");
        $this->assertEquals(false,$user->isValidFullName(),"Return false if firstname  and lastname are both empty");
    }

    //Validation's Tests + Null Attributes

    public function testValidUser()
    {
        $user = new User("samuel","benitah",23,"sambenitah@gmail.com","samsamsam");
        $this->assertEquals(true,$user->isValid(),"Return true if user is valid");
    }

    public function testEmptyUserFirstName()
    {
        $user = new User("","benitah",23,"sambenitah@gmail.com","samsamsam");
        $this->assertEquals(false,$user->isValid(),"Return false if user's firstname is empty");
    }

    public function testEmptyUserLastName()
    {
        $user = new User("samuel","",23,"sambenitah@gmail.com","samsamsam");
        $this->assertEquals(false,$user->isValid(),"Return false if user's lastname is empty");
    }

    public function testUnValidUserEmail()
    {
        $user = new User("samuel","benitah",23,"sambenitahgmail.com","samsamsam");
        $this->assertEquals(false,$user->isValid(),"Return false if user's email is not valid");
    }

    public function testUnValidUserPassword()
    {
        $user = new User("samuel","benitah",23,"sambenitah@gmail.com","ss");
        $this->assertEquals(false,$user->isValid(),"Return false if user's password is not valid");
    }

    public function testUnValidUserAge()
    {
        $user = new User("samuel","benitah",12,"sambenitah@gmail.com","samsamsam");
        $this->assertEquals(false,$user->isValid(),"Return false if user's age is not valid");
    }

    public function testNullAttributes()
    {
        $this->expectException(ArgumentCountError::class);
        new User("samuel","benitah",23,"sambenitah@gmail.com");

    }

    //TodoList's Tests

    public function testCreateTodoList()
    {
        $user = new User("samuel","benitah",23,"sambenitah@gmail.com","samsamsam");
        $user->createTodoList();
        $this->assertEquals(true,$user->hasTodoList(),"Return true if user can create a TodoList");
    }


    public function testAddNewTodoList()
    {
        $user = new User("samuel","benitah",23,"sambenitah@gmail.com","samsamsam");
        $user->createTodoList();
        $this->assertEquals(false,$user->createTodoList(),"Return false if an user have already a TodoList and can't create another one");
    }

    public function testHasTodoList()
    {
        $user = new User("samuel","benitah",23,"sambenitah@gmail.com","samsamsam");
        $this->assertEquals(false,$user->hasTodoList(),"Return false if an user dont have a TodoList");
    }





}