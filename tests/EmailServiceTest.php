<?php


use SpaceCowboy\EmailService;
use SpaceCowboy\User;

class EmailServiceTest extends PHPUnit\Framework\TestCase
{

    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->user = new User("samuel","benitah",23,"sambenitah@gmail.com","samsamsam");
    }

    public function testSendMock()
    {
        $email = $this->createMock(EmailService::class);

        $email->expects($this->any())
            ->method('send')
            ->will($this->returnValue(false));

        $this->assertEquals(false,$email->send($this->user),"Return False if the Mail cannot be send");
    }


    public function testSendUser()
    {
        $email = new EmailService();
        $this->assertEquals(true,$email->send($this->user),"Return True if the User have more than 18 years old");
    }


    public function testCantSendUser()
    {
        $email = new EmailService();
        $this->user->age = 17;
        $this->assertEquals(false,$email->send($this->user),"Return False if the User have less than 18 years old");
    }

    public function testCannottSendUser()
    {
        $email = new EmailService();
        $this->user->age = 18;
        $this->assertEquals(false,$email->send($this->user),"Return False if the User have 18 years old");
    }

}